'''
This file contains the various functions defined for the explicit purposes of replaying the data from the DAQ.

The idea is that these are called directly after the data is written from the Windows Fast-DAQ.
Then the output from these scripts is actually what pyNab expects to handle. 
'''
#standard python imports
import numpy as np
import h5py #used for file reading/writing
import os #used for directory and file functions
import time #used for sleep function
import gc
import shutil
from numba import jit
import glob
import ast
import concurrent
import pandas as pd

#custom code imports
from . import basicFunctions as bf
from . import fileFormats as ff

compressionAvailable = False
try:
	import deltaRice.h5
	compressionAvailable = True
except Exception as e:
    compressionAvailable = False

#--------------------------------------------#
#				REPLAY CLASS
#--------------------------------------------#
class replay:
	"""
	Manages input parameters for replaying.
	This class object is instantiated at the execution of the replay script.
	
	User passes in parameters as keyword arguments (kwargs).
	Parameters
	----------
	inputDir: string
		Defaults to: /mnt/NabRaid0/Nab/temp/
		The location to look for incoming files
	outputDir: string
		Defaults to: /mnt/NabRaid0/Nab/ReplayOutput/
		The location to output files to
	badDir: string
		Defaults to: /mnt/NabRaid0/Nab/ProblemFiles/
		The location where problematic files that broke the replay script should be moved to
		Any files that fail to replay, or replayed files that fail to verify, are moved here
	saveOrigDir: string
		Defaults to: None
		Directory for saving pre-replayed files
		Don't include if not saving pre-replayed files
	queryTime: int
		Defaults to 5
		The time to wait to look for new files if no files were found
	compression: bool
		Defaults to: False
		Enables compression via deltaRice
	batchSize: int
		Defaults to: 1024
		The number of waveforms per chunk in the output dataset
	m: int
		Defaults to: 8
		Golomb coding parameter used in the deltaRice function
	numProcs: int
		Defaults to: 1
		How many files to replay simultaneously
		Limit this to below 4
	specificFile: string
		Defaults to: None
		The basename (no extension) for a particular file to replay.
		Will ignore the .end file search and go straight to replaying.
	runLog: str
		Defaults to: None
		Full filename and path to the CSV file used for run logging.
		Ignore this to disable run logging.
	"""
	def __init__(rep, **kwargs):
		"""
		Defines the replay parameters.
		"""
		print("Replay code initializing...")
		rep.inputDir = kwargs.get('inputDir', '/mnt/NabRaid0/Nab/temp/')
		rep.outputDir = kwargs.get('outputDir', '/mnt/NabRaid0/Nab/ReplayOutput/')
		rep.badDir = kwargs.get('badDir', '/mnt/NabRaid0/Nab/ProblemFiles/')
		rep.saveOrigDir = kwargs.get('saveOrigDir', None) # This one defaults to None
		rep.saveOrig = bool(rep.saveOrigDir)
		rep.queryTime = kwargs.get('queryTime', 5)
		rep.compression = kwargs.get('compression', False)
		rep.batchSize = kwargs.get('batchSize', 1024)
		rep.m = kwargs.get('m', 8)
		rep.numProcs = kwargs.get('numProcs', 1)
		if rep.numProcs > 1:
			print("\t\tWarning: numProcs > 1 not yet supported!")
		rep.specificFile = kwargs.get('specificFile', None)
		if rep.specificFile:
			if '_' not in rep.specificFile:
				print("\t\tWarning: specific file not formatted correctly! Needs: #_#")
				print("\t\tRunning as normal.")
				rep.specificFile = None
		rep.runlog = kwargs.get('runLog', None)
		if rep.runlog:
			if rep.runlog.split('.')[-1] != 'csv': # If this isn't a CSV file
				print("\t\tWarning: run log is not a CSV file! Disabling logging.")
				rep.runlog = None
		rep.functions = [ # List out the replay functions. Useful for looping through them
			getattr(inputData, 'createTemperature'),
			getattr(inputData, 'createParameter'),
			getattr(inputData, 'createTrigger'),
			getattr(inputData, 'createEvent'),
			getattr(inputData, 'createWaveforms'),
			getattr(inputData, 'updateRunLog')
		]
		print("\tReplay parameters accepted and set.")
	
	def replayHandler(rep):
		"""
		Handles the replaying after class instantiation.
		"""
		if not rep.specificFile: # The case where a specific file was not specified
			print(f"\tLooking for files in {rep.inputDir}")
			print(f"\tAllow for up to {rep.queryTime} seconds for code to stop when signal is received.")
			res = rep.__go()
			resStr = "no errors"
			if res != 0:
				resStr = "errors"
			print(f"Replay code stopped with {resStr}.")
			return
		else: # The case where a specific file was specified
			start = time.time()
			print(f"\tReplaying specific file Run{rep.specificFile}")
			try:
				replaying = inputData(f"{rep.inputDir}Run{rep.specificFile}.h5", rep.outputDir)
			except Exception as e:
				replaying = -1
				print(f"\t\tFailed to instantiate input file class.")
				print(f"\t\t{e}")
				res = -1
			else:
				print(f"\t\tInstantiated input file for {rep.specificFile}")
				# Loop through the replay functions
				res = 0
				for func in rep.functions:
					# If one of the functions fails, stop replaying.
					if res != 0:
						break
					else:
						if func == getattr(inputData, 'createWaveforms'):
							res = func(replaying, rep.compression, rep.m, rep.batchSize)
						elif func == getattr(inputData, 'updateRunLog'):
							if rep.runlog:
								_ = func(replaying, rep.runlog)
							else:
								continue
						else:
							res = func(replaying)
			if res != 0:
				print(f"\t\tFailed to replay {rep.specificFile}")
				print(f"\t\t\tMoving un-replayed files to {rep.badDir}")
				if isinstance(replaying, inputData):
					# Only do this stuff if the inputData class object is defined
					for b in replaying.fileList:
						base = os.path.basename(b)
						shutil.move(b, rep.badDir+base)
					del replaying
				else:
					# If it failed to start, we'll have to build the file list here
					waveFiles = glob.glob(f"{rep.inputDir}{rep.specificFile}RIO*.wave") # Grab all the binary files
					fileList = [
						f,
						f"{rep.inputDir}{rep.specificFile}.trig",
						f"{rep.inputDir}{rep.specificFile}.evnt",
					] + waveFiles
					for b in fileList:
						bn = os.path.basename(b)
						shutil.move(b, rep.badDir+bn)
				resStr = "errors"
			else:
				print(f"\tSuccessfully replayed {rep.specificFile} in {round(time.time()-start,4)}s.")
				resStr = "no errors"
				handling = 'handling'
				try:
					for fl in replaying.fileList:
						base = os.path.basename(fl)
						if rep.saveOrig:
							handling = 'saving'
							shutil.move(fl, rep.saveOrigDir+base)
						else:
							handling = 'deleting'
							os.remove(fl)
				except Exception as e:
					print(f"\t\tTrouble {handling} the pre-replayed files!")
					print(f"\t\t{e}")
					resStr = "errors"
				del replaying
			rep.__deleteEndFile(f"{rep.inputDir}Run{rep.specificFile}.end") # Delete the .end file
			print(f"Replay code stopped with {resStr}.")
			return
			
	def __go(rep):
		"""
		The function that loops continuously until 'stop.stop' is found in inputDir.
		No input required.
		
		Returns
		-------
		-1 if an error occurred.
		0 otherwise.
		"""
		stop = False
		while not stop:
			files = glob.glob(f"{rep.inputDir}*.end") # Grab the list of .end files in the input directory
			toOperateOn = rep.__findHDF5FilesToOperateOn(files) # Find which files to replay
			if toOperateOn is not None:
				if isinstance(toOperateOn, list):
					# We're good to go; start looping through the files
					for f in toOperateOn:
						name = os.path.basename(f).split('.')[0]
						start = time.time()
						print(f"\t\tReplaying {name}")
						res = 0
						try:
							replaying = inputData(f, rep.outputDir)
						except Exception as e:
							replaying = -1
							print(f"\t\t\tFailed to instantiate input file class.")
							print(f"\t\t\t{e}")
							res = -1
						else:
							print(f"\t\t\tInstantiated input file for {name}")
							# Loop through the replay functions
							for func in rep.functions:
								# If one of the functions fails, stop replaying.
								if res != 0:
									break
								else:
									if func == getattr(inputData, 'createWaveforms'):
										res = func(replaying, rep.compression, rep.m, rep.batchSize)
									elif func == getattr(inputData, 'updateRunLog'):
										if rep.runlog:
											_ = func(replaying, rep.runlog)
										else:
											continue
									else:
										res = func(replaying)
						if res != 0:
							print(f"\t\tFailed to replay {name}")
							print(f"\t\t\tMoving un-replayed files to {rep.badDir}")
							if isinstance(replaying, inputData):
								# Only do this stuff if the inputData class object is defined
								for b in replaying.fileList:
									base = os.path.basename(b)
									shutil.move(b, rep.badDir+base)
								del replaying
							else:
								# If it failed to start, we'll have to build the file list ourselves
								base = os.path.basename(f).split('.')[0]
								waveFiles = glob.glob(f"{rep.inputDir}{base}RIO*.wave") # Grab all the binary files
								fileList = [
									f,
									f"{rep.inputDir}{base}.trig",
									f"{rep.inputDir}{base}.evnt",
								] + waveFiles
								for b in fileList:
									bn = os.path.basename(b)
									shutil.move(b, rep.badDir+bn)
						else:
							print(f"\t\tSuccessfully replayed {name} in {round(time.time()-start,4)}s.")
							handling = 'handling'
							try:
								for fl in replaying.fileList:
									base = os.path.basename(fl)
									if rep.saveOrig:
										handling = 'saving'
										shutil.move(fl, rep.saveOrigDir+base)
									else:
										handling = 'deleting'
										os.remove(fl)
							except Exception as e:
								print(f"\t\t\tTrouble {handling} the pre-replayed files!")
								print(f"\t\t\t{e}")
							del replaying
						rep.__deleteEndFile(f) # Delete the .end file
						stop = 'stop.stop' in files # Check for stop.stop in input directory
						if stop: # If stop signal was found, go ahead and stop replaying
							break
				else: # In this case, something went wrong while gathering files
					print("\t\tSomething went wrong gathering the HDF5 files.")
					stop = True
					res = -1
			else:
				time.sleep(rep.queryTime)
			if not stop: # Check if it isn't already set to true
				stop = os.path.exists(f'{rep.inputDir}stop.stop') # Check for stop.stop in input directory
			if stop:
				print("\tStopping.")
				os.remove(os.path.join(rep.inputDir, 'stop.stop')) # Remove the stop file if it was received.
				res = 0
		return res
		
	def __findHDF5FilesToOperateOn(rep, files):
		"""
		Returns file names that have a .end file.
		"""
		if len(files) == 0: # No files found
			return None
		else:
			h5files = []
			for f in files:
				name = os.path.splitext(f)[0] # Grab the name of the file
				#h5name = os.path.join(rep.inputDir, name) + '.h5' # Form the corresponding HDF5 file path
				h5name = name + '.h5' # Form the corresponding HDF5 file path
				if os.path.exists(h5name): # Check that it exists
					h5files.append(h5name)
				else:
					print(f"\t\t{name}.end does not have corresponding HDF5 file!")
					print("\t\tGetting rid of the .end file.")
					rep.__deleteEndFile(f) # Just delete it...
			return sorted(h5files, key=os.path.getmtime)
	
	def __deleteEndFile(rep, file):
		"""
		Looks for a .end file corresponding to the input file and removes it.
		Generally expects the HDF5 filename as input.
		"""
		try:
			name = os.path.splitext(file)[0]
			os.remove(f"{name}.end")
		except Exception as e:
			print(f"\t\t{e}")
			return
		else:
			return

#--------------------------------------------#
#				INPUT DATA CLASS
#--------------------------------------------#
class inputData:
	"""
	Wraps information from the input data into a class object.
	This class object is instantiated every time there is a new file to be replayed.
	
	Parameters
	----------
	file: string
		The input HDF5 file (with full path).
	outLoc: string
		The output location for the replayed file.
	
	Parsing is started when the class is insantiated.
	"""
	def __init__(inp, file, outLoc):
		inp.fpath = file.split("Run")[0] # Sets the file location
		inp.base = os.path.basename(file).split('.')[0]
		inp.runNum = int(inp.base.split('Run')[-1].split('_')[0])
		inp.waveFiles = glob.glob(f"{inp.fpath}{inp.base}RIO*.wave") # Grab all the binary files
		inp.fileList = [
			file,
			f"{inp.fpath}{inp.base}.trig",
			f"{inp.fpath}{inp.base}.evnt"
		] + inp.waveFiles
		print(f"\t\t\t\tParsing waveforms...")
		start = time.time()
		inp.waveforms = waveData(inp.waveFiles) # Create a subclass for the waveform data
		print(f"\t\t\t\tWaveforms parsed in {round(time.time()-start,4)}s.")
		inp.trigData = np.fromfile( # Read in the trigger file
			f"{inp.fpath}{inp.base}.trig",
			dtype=ff.triggerType(version='reading')
		)
		inp.evntData = np.fromfile( # Read in the event file
			f"{inp.fpath}{inp.base}.evnt",
			dtype=ff.eventType(version=1)
		)
		
		inp.h5in = h5py.File(file, 'r') # Attempt to open the HDF5 input file.
		# Do some organizing of run files
		runs = inp.runNum-inp.runNum%100 # Grab the set of 100 runs this belongs to
		if ":" in outLoc: # Assuming this means we're on Windows
			path = f"{outLoc}Runs{runs}\\"
		else:
			path = f"{outLoc}Runs{runs}/"
		if not os.path.exists(path): # Make the directory if it doesn't exist yet
			os.mkdir(path)
		inp.h5out = h5py.File(f"{path}{inp.base}.h5", 'w') # Open up a new HDF5 file for output.
		
	def __str__(inp):
		return f"Input file class object for replaying {file}"
	
	def createTemperature(inp):
		"""
		Creates an FPGA Temperatures dataset in the output HDF5 file from the input HDF5 file.
		
		Returns
		-------
		-1 if an error occurred. 0 otherwise.
		"""
		print("\t\t\tStarting FPGA Temperatures")
		try:
			if inp.h5in['FPGATemperatures'].shape != 0: # if it's not empty, make this
				inp.h5out.create_dataset('FPGATemperatures', data = inp.h5in['FPGATemperatures'], fletcher32=True)
		except Exception as e:
			print(f"\t\t\t{e}")
			return -1
		else:
			return 0
	
	def createParameter(inp):
		"""
		Creates a Parameter group in the output HDF5 file from the input HDF5 file.
		Loops through the input Parameter group and creates a corresponding subgroup/dataset in the output HDF5 file.
		
		Returns
		-------
		-1 if an error occurred. 0 otherwise.
		"""
		print("\t\t\tStarting Parameters")
		res = 0
		for key in inp.h5in['Parameters'].keys():
			datasetName = f'Parameters/{key}'
			try: # Attempt to just write out the data into the output file
				inp.h5out[datasetName] = inp.h5in[datasetName][()]
			except TypeError: # If that failed, it's probably a group
				group = inp.h5in[datasetName]
				for k in group.keys():
					sname = f"{datasetName}/{k}"
					try:
						inp.h5out[sname] = group[k][()]
					except: # Say screw it and set it to 0
						inp.h5out[sname] = 0
				del group
			except Exception as e:
				print(f"\t\t\t{e}")
				res = -1
				break
		return res
		
	def createTrigger(inp):
		"""
		Creates a Trigger dataset in the output HDF5 file from the input HDF5 file.
		
		Returns
		-------
		-1 if an error occurred. 0 otherwise.
		"""
		print("\t\t\tStarting Triggers")
		try:
			inp.h5out.create_dataset('triggers', data = inp.trigData, fletcher32=True)
		except Exception as e:
			print(f"\t\t\t{e}")
			return -1
		else:
			return 0
			
	def createEvent(inp):
		"""
		Creates an Event dataset in the output HDF5 file from the input HDF5 file.
		
		Returns
		-------
		-1 if an error occurred. 0 otherwise.
		"""
		print("\t\t\tStarting Events")
		try:
			inp.h5out.create_dataset('events', data = inp.evntData, fletcher32=True)
		except Exception as e:
			print(f"\t\t\t{e}")
			return -1
		else:
			return 0
			
	def createWaveforms(inp, compression, m, batchSize):
		"""
		Creates a waveform group in the output HDF5 file from the input HDF5 file.
		Creates a subgroup with a headers and waveforms dataset for each waveform type.
		
		Parameters
		----------
		compression: bool
			Specify whether or not to use deltaRice compression.
		m: int
			Golomb coding parameter used in the deltaRice function
		batchSize: int
			The number of waveforms per chunk in the output dataset
		
		Returns
		-------
		-1 if an error occurred. 0 otherwise.
		"""
		start = time.time() # We're going to keep track of how long this takes.
		print("\t\t\tStarting Waveforms")
		try:
			for name,waves in zip(inp.waveforms.types, inp.waveforms.parsed):
				if waves is None: # If it's empty, fill it in with something
					inp.h5out.create_dataset(
						f"waveforms/{name}/headers",
						data = h5py.Empty(inp.waveforms.headertype)
					)
					inp.h5out.create_dataset(
						f"waveforms/{name}/waveforms",
						data = h5py.Empty('int16')
					)
				else:
					# Create the headers
					inp.h5out.create_dataset(
						f"waveforms/{name}/headers",
						data = waves[0],
						fletcher32=True
					)
					# Create the waveform dataset
					if not compression or not compressionAvailable:
						inp.h5out.create_dataset(
							f"waveforms/{name}/waveforms",
							data = waves[1],
							fletcher32=True
						)
					else: # If compressing
						defSize = batchSize
						if defSize > waves[1].shape[0]:
							defSize = waves[1].shape[0]
						chunks = (defSize, waves[1].shape[1])
						inp.h5out.create_dataset(
							f"waveforms/{name}/waveforms",
							data = waves[1],
							chunks = chunks,
							compression = deltaRice.h5.H5FILTER,
							compression_opts = (m, waves[1].shape[1]),
							fletcher32=True
						)
		except Exception as e:
			print(f"\t\t\t{e}")
			del inp.waveforms
			return -1
		else:
			print(f"\t\t\tCompleted waveform writing in {round(time.time()-start,4)}s.")
			del inp.waveforms # We're done with this, so get rid of it
			return 0
	
	def updateRunLog(inp, runlog):
		"""
		Updates a run log CSV file with run number, descrption, start time, and other meta data.
		
		Parameters
		----------
		runlog: str
			Full path and name to CSV file used for run logging.
		
		Returns
		-------
		-1 if an error occurred. 0 otherwise.
		"""
		print("\t\t\tUpdating Run Log")
		relevantData = [ # List out the relevant data to include in the run log
			'DAQStability',
			'Errors',
			'FullScaleVoltageRange',
			'GitInformation',
			'HardwareConfiguration',
			'RunDescription',
			'RunMetadata',
			'RunSettings',
			'RunTime'
		]
		try:
			# First check in the run log if this run has already been logged
			dup = False
			df = pd.DataFrame()
			if os.path.isfile(runlog): # If the run log exists
				df = pd.read_csv(runlog,
					header=0,
					usecols=["Run"],
					index_col=["Run"]
				)
				if inp.runNum in df.index:
					dup = True
			errs = inp.h5in['Parameters/Errors'][()]
			errCode = errs[1]
			err = errs[2].decode().replace("\n", " ").replace("\t", " ").replace("\r", " ") # Replace character returns and tabs with spaces
			if errCode != 5004 and err:  # Update the run log if it hasn't already been documented UNLESS there was an error, then update regardless
				dup = False
			if not dup:
				dfData = {'Run':[inp.runNum]} # Create a dict for making the dataframe
				# If it's not a duplicate, extract the relevant parameters
				main = inp.h5in['Parameters']
				for mk in main.keys():
					if mk not in relevantData:
						continue
					try:
						d = main[mk][()]
						if mk == 'Errors':
							# This is a special case where the sub run ended okay, so we'll say there was no error.
							if errCode == 5004 or not err:
								errStat, errCode, err = 0,0,"No error."
							else:
								errStat = d[0]
							dfData.update({"ErrorStatus": errStat})
							dfData.update({"ErrorCode": errCode})
							dfData.update({"ErrorDescription": err})
						else:
							if type(d) == bytes:
								d = d.decode()
							dfData.update({mk:[d]})
					except TypeError: # This error means it's probably a group
						group = main[mk]
						for key in group.keys():
							try:
								d = group[key][()]
								if type(d) == bytes:
									d = d.decode()
								dfData.update({key:[d]})
							except TypeError: # Putting in another layer just in case...
								subgroup = group[key]
								for k in subgroup.keys():
									d = subgroup[k][()] # If it doesn't work past this point, something's wrong
									if type(d) == bytes:
										d = d.decode()
									dfData.update({k:[d]})
								del subgroup
						del group
				del main
				df = pd.DataFrame.from_dict(dfData) # Create a Pandas dataframe
				df.to_csv(runlog, mode='a', header=not os.path.exists(runlog), index=False)
		except Exception as e:
			print(f"\t\t\t{e}")
			print(f"\t\t\t\tError. Attempting to do minimal run log update.")
			try:
				df = pd.DataFrame.from_dict({"Run": [inp.runNum]})
				df.to_csv(runlog, mode='a', header=not os.path.exists(runlog), index=False)
			except:
				print(f"\t\t\t\tFailed minimal run log update.")
			if not df.empty:
				del df
			return -1
		else:
			if not df.empty:
				del df
			return 0
	
	def __del__(inp):
		"""
		Should be called when file is finished replaying.
		"""
		try:
			inp.h5in.close()
			inp.h5out.close()
		except Exception as e:
			print(f"\t\t\tError closing the HDF5 files.")
			print(f"\t\t\t{e}")

#--------------------------------------------#
#				WAVEFORM DATA CLASS
#--------------------------------------------#
class waveData:
	"""
	This class organizes incoming waveform data.
	Upon instantiation, input waveform files are parsed and separated into the different waveform types.
	"""
	def __init__(wav, files):
		"""
		Instantiates the waveform data class.
		
		Parameters
		----------
		files: list
			List of waveform binary (.wave) files to read in.
		"""
		# Take note of the formatting
		wav.headertype = np.dtype(ff.headerType(fileFormat='Nab', formatNumber=4))
		wav.files = files
		wav.types = [ # All the different waveform types
			'singles',
			'coincidences',
			'pulsers',
			'baselines',
			'cosmics'
		]
		# Parse all the files
		wav.parsed = wav.__parseFiles()
	
	def __parseFiles(wav):
		"""
		Goes through all files and sorts data into each waveform type.
		
		Returns
		-------
		
		"""
		# Set the output containers
		singles = []
		singlesChecksums = []
		coincidences = []
		coincidencesChecksums = []
		pulsers = []
		pulsersChecksums = []
		baselines = []
		baselinesChecksums = []
		cosmics = []
		cosmicsChecksums = []
		for f in wav.files:
			wdata = np.fromfile(f, dtype=np.uint16) # Extract the binary data from the file
			s, sChecks, c, cChecks, p, pChecks, b, bChecks, cos, cosChecks = wav.__separateWaveformTypes(wdata)
			del wdata # We're done with this
			if s is not None:
				singles.append(s)
				singlesChecksums.append(sChecks)
			if c is not None:
				coincidences.append(c)
				coincidencesChecksums.append(cChecks)
			if p is not None:
				pulsers.append(p)
				pulsersChecksums.append(pChecks)
			if b is not None:
				baselines.append(b)
				baselinesChecksums.append(bChecks)
			if cos is not None:
				cosmics.append(cos)
				cosmicsChecksums.append(cosChecks)
		# They're all read in, now to prep them.
		if len(singles) > 0:
			# Flatten them all into one array
			singles = np.concatenate(singles)
			singlesChecksums = np.concatenate(singlesChecksums)
			if singles.ndim == 2:
				wavelength = singles.shape[1] - 13
				# Note the formatting
				dtype = ff.headWaveType(wavelength, 'Nab', 4)
				singles = singles.view(dtype)[:,0]
				# Update the waveform object with checksumming included.
				singles = wav.__prepareDataset(singles, singlesChecksums)
			else:
				singles = None
		if len(coincidences) > 0:
			coincidences = np.concatenate(coincidences)
			coincidencesChecksums = np.concatenate(coincidencesChecksums)
			if coincidences.ndim == 2:
				wavelength = coincidences.shape[1] - 13
				dtype = ff.headWaveType(wavelength, 'Nab', 4)
				coincidences = coincidences.view(dtype)[:,0]
				coincidences = wav.__prepareDataset(coincidences, coincidencesChecksums)
			else:
				coincidences = None
		else:
			coincidences = None
		if len(pulsers) > 0:
			pulsers = np.concatenate(pulsers)
			pulsersChecksums = np.concatenate(pulsersChecksums)
			if pulsers.ndim == 2:
				wavelength = pulsers.shape[1] - 13
				dtype = ff.headWaveType(wavelength, 'Nab', 4)
				pulsers = pulsers.view(dtype)[:,0]
				pulsers = wav.__prepareDataset(pulsers, pulsersChecksums)
			else:
				pulsers = None
		else:
			pulsers = None
		if len(baselines) > 0:
			baselines = np.concatenate(baselines)
			baselinesChecksums = np.concatenate(baselinesChecksums)
			if baselines.ndim == 2:
				wavelength = baselines.shape[1] - 13
				dtype = ff.headWaveType(wavelength, 'Nab', 4)
				baselines = baselines.view(dtype)[:,0]
				baselines = wav.__prepareDataset(baselines, baselinesChecksums)
			else:
				baselines = None
		else:
			baselines = None
		if len(cosmics) > 0:
			cosmics = np.concatenate(cosmics)
			cosmicsChecksums = np.concatenate(cosmicsChecksums)
			if cosmics.ndim == 2:
				wavelength = cosmics.shape[1] - 13
				dtype = ff.headWaveType(wavelength, 'Nab', 4)
				cosmics = cosmics.view(dtype)[:,0]
				cosmics = wav.__prepareDataset(cosmics, cosmicsChecksums)
			else:
				cosmics = None
		else:
			cosmics = None
		return singles, coincidences, pulsers, baselines, cosmics
		
	@jit
	def __separateWaveformTypes(wav, wdata):
		"""
		Reads through the binary file and separates out the different waveform types.
		Types are:
			singles
			coincidences
			pulsers
			baselines
			cosmics
		
		Parameters
		----------
		wdata: np.ndarray
			Binary waveform data to be separated out.
		
		Returns
		-------
		waveforms: np.ndarray
			Array with waveforms for a particular type
			Returns None if empty
			One for each type; 5 total
		checksums: np.ndarray
			Array with checksums for each waveform of a particular type
			Returns None if empty
			One for each type; 5 total
		"""
		curloc = 0
		headerlength = 13
		stoploc = len(wdata)
		
		# Set up the output containers
		singles = []
		singlesChecksums = []
		coincidences = []
		coincidencesChecksums = []
		pulsers = []
		pulsersChecksums = []
		baselines = []
		baselinesChecksums = []
		cosmics = []
		cosmicsChecksums = []
		
		# Walk through the file
		while curloc < stoploc:
			# First grab the waveform length
			length = wdata[curloc] - headerlength
			# Step up one step
			curloc += 1
			# Now read the header itself
			header = wdata[curloc:curloc+headerlength].view(wav.headertype)[0]
			# Grab the type of waveform
			eventType = header['event type']
			# Compute the checksum (of just the waveform)
			checksum = bf.Fletcher32(bf.doAnd(wdata[curloc+headerlength:curloc+length+headerlength]).view(np.uint16))
			# Grab the corresponding waveform (including header)
			thiswave = wdata[curloc:curloc+length+headerlength]
			# Put this information in the appropriate container
			if eventType == 0: # Means it's a singles
				singles.append(thiswave)
				singlesChecksums.append(checksum)
			elif eventType == 1: # Means it's a coincidence
				coincidences.append(thiswave)
				coincidencesChecksums.append(checksum)
			elif eventType == 2: # Means it's a pulser
				pulsers.append(thiswave)
				pulsersChecksums.append(checksum)
			elif eventType == 3: # Means it's a baseline trace
				baselines.append(thiswave)
				baselinesChecksums.append(checksum)
			elif eventType == 4: # Means it's a cosmic
				cosmics.append(thiswave)
				cosmicsChecksums.append(checksum)
			curloc += length+headerlength # Move up to the next one
		
		# Repackage these as arrays (if there's anything there)
		if len(singles) > 0:
			singles = np.asarray(singles)
			singlesChecksums = np.asarray(singlesChecksums).astype(np.uint32)
		else:
			singles = None
			singlesChecksums = None
		if len(coincidences) > 0:
			coincidences = np.asarray(coincidences)
			coincidencesChecksums = np.asarray(coincidencesChecksums).astype(np.uint32)
		else:
			coincidences = None
			coincidencesChecksums = None
		if len(pulsers) > 0:
			pulsers = np.asarray(pulsers)
			pulsersChecksums = np.asarray(pulsersChecksums).astype(np.uint32)
		else:
			pulsers = None
			pulsersChecksums = None
		if len(baselines) > 0:
			baselines = np.asarray(baselines)
			baselinesChecksums = np.asarray(baselinesChecksums).astype(np.uint32)
		else:
			baselines = None
			baselinesChecksums = None
		if len(cosmics) > 0:
			cosmics = np.asarray(cosmics)
			cosmicsChecksums = np.asarray(cosmicsChecksums).astype(np.uint32)
		else:
			cosmics = None
			cosmicsChecksums = None
		return singles, singlesChecksums, coincidences, coincidencesChecksums, pulsers, pulsersChecksums, baselines, baselinesChecksums, cosmics, cosmicsChecksums
	
	def __prepareDataset(wav, waves, checksums):
		"""
		Prepares waveform data for writing to an HDF5 file:
		 - Sorts by event ID
		 - Calculates fletcher32 checksum for each value
		 - Bitshift the waveform data
		
		Parameters
		----------
		waves: np.ndarray
			Array with waveform data that needs prepared.
		checksums: np.ndarray
			Checksums associated with the waveform data.
		
		Returns
		-------
		newHeaders: np.ndarray
			Updated headers with checksums.
		waveforms: np.ndarray
			Updated and sorted waveforms
		"""
		# Sort by event ID
		indices = np.argsort(waves['header']['eventid'])
		newHeaderType = []
		for element in waves['header'].dtype.descr:
			newHeaderType.append(element)
		newHeaderType.append(('checksum', 'u4'))
		newHeaders = np.zeros(len(indices), dtype=np.dtype(newHeaderType))
		for name in waves['header'].dtype.names:
			newHeaders[name] = np.copy(waves['header'][name][indices])
		newHeaders['checksum'] = np.copy(checksums[indices])
		temp = bf.doAnd(waves['wave']) # do the bitshift
		waveforms = temp[indices]
		del waves
		del checksums
		del indices
		return newHeaders, waveforms
