'''
This file contains the various functions defined for the explicit purposes of replaying the data from the DAQ.

The idea is that these are called directly after the data is written from the Windows Fast-DAQ.
Then the output from these scripts is actually what pyNab expects to handle. 
'''
#standard python imports
import numpy as np
import h5py #used for file reading/writing
import os #used for directory and file functions
import time #used for sleep function
import gc
import shutil
from numba import jit
import glob
import ast
import concurrent
import pandas as pd

#custom code imports
from . import basicFunctions as bf
from . import fileFormats as ff

__compressionAvailable = False
try:
	import deltaRice.h5
	__compressionAvailable = True
except Exception as e:
    __compressionAvailable = False

def getHeaderType(dataIn):
	output = None
	if len(dataIn) != 0:
		dtype = dataIn.dtype
		if dtype == 'uint16':
			header = [('bc', 'u1')]
			header.append(('result', 'u1'))
			header.append(('timestamp', 'u8'))
			header.append(('req', 'u8'))
			header.append(('hit type', 'u1'))
			header.append(('event type', 'u1'))
			header.append(('blank', 'u2'))
			header.append(('eventid', 'u4'))
			shape = dataIn.shape
			if len(shape) == 1:
				waveformLength = int(shape[0]-13)
			else:
				waveformLength = int(shape[1]-13)
			waveform = str(waveformLength)+'u2'
			return np.dtype(header), waveform, True
		else:
			headerType = []
			for name in dtype.names[:-1]:
				headerType.append((name, dtype[name]))
			return np.dtype(headerType), None, False
	else:
		return None

def convertWaveformType(dataIn):
	output = None
	if len(dataIn) != 0:
		headerType, waveformType, flat = getHeaderType(dataIn)
		if flat == True: #in this case we are dealing with the flattened structure so use numpy.view to parse it all
			outType = np.dtype([('header', headerType), ('waveform', waveformType)])
			return dataIn.view(outType)
		else:
			headerType = getHeaderType(dataIn)
			outType = [('header', headerType)]
			outType.append(('waveform', str(len(dataIn[0]['waveform']))+'h'))
			output = np.zeros(len(dataIn), dtype=outType)
			for name in headerType.names:
				output['header'][name][:] = dataIn[name]
			temp = np.vstack(dataIn['waveform']) #stack all the waveform data together, this loads it in nicely
			del dataIn
			gc.collect()
			temp[:] = bf.doAnd(temp) #do the 16 bit conversion process
			gc.collect()
			output['waveform'][:] = temp.astype(np.int16)
			del temp
			gc.collect()
	return output

@jit
def separateWaveformTypes(file, headertype):
	#this function is passed an array of unsigned short values
	curloc = 0
	headerlength = 13
	stoploc = len(file)
	#setup the output containers
	singles = []
	singlesChecksums = []
	coincidences = []
	coincidencesChecksums = []
	pulsers = []
	pulsersChecksums = []
	baselines = []
	baselinesChecksums = []
	cosmics = []
	cosmicsChecksums = []
	while curloc < stoploc:
		#first grab the length of the waveform
		length = file[curloc] - 13
		#step past that
		curloc += 1
		#now this is the header itself
		header = file[curloc:curloc+headerlength].view(headertype)[0]
		#check what type of waveform it is and depending on that send it somewhere else
		#at this stage the headers and waveforms have not been modified, just sent somewhere
		eventType = header['event type']
		tempWaveform = bf.doAnd(file[curloc+headerlength:curloc+length+headerlength])
		checksum = bf.Fletcher32(tempWaveform.view(np.uint16))
		if eventType == 0:
			singles.append(file[curloc:curloc+length+headerlength])
			singlesChecksums.append(checksum)
		elif header['event type'] == 1:
			coincidences.append(file[curloc:curloc+length+headerlength])
			coincidencesChecksums.append(checksum)
		elif header['event type'] == 2:
			pulsers.append(file[curloc:curloc+length+headerlength])
			pulsersChecksums.append(checksum)
		elif header['event type'] == 3:
			baselines.append(file[curloc:curloc+length+headerlength])
			baselinesChecksums.append(checksum)
		elif header['event type'] == 4:
			cosmics.append(file[curloc:curloc+length+headerlength])
			cosmicsChecksums.append(checksum)
		curloc += length + headerlength
	return singles, singlesChecksums, coincidences, coincidencesChecksums, pulsers, pulsersChecksums, baselines, baselinesChecksums, cosmics, cosmicsChecksums

def parseFile(filename, headertype):
	start = time.time()
	file = np.fromfile(filename, dtype=np.uint16)
	singles, singlesChecksums, coincidences, coincidencesChecksums, pulsers, pulsersChecksums, baselines, baselinesChecksums, cosmics, cosmicsChecksums = separateWaveformTypes(file, headertype)
	if len(singles) > 0:
		singles = np.asarray(singles)
		singlesChecksums = np.asarray(singlesChecksums).astype(np.uint32)
	else:
		singles = None
		singlesChecksums = None
	if len(coincidences) > 0:
		coincidences = np.asarray(coincidences)
		coincidencesChecksums = np.asarray(coincidencesChecksums).astype(np.uint32)
	else:
		coincidences = None
		coincidencesChecksums = None
	if len(pulsers) > 0:
		pulsers = np.asarray(pulsers)
		pulsersChecksums = np.asarray(pulsersChecksums).astype(np.uint32)
	else:
		pulsers = None
		pulsersChecksums = None
	if len(baselines) > 0:
		baselines = np.asarray(baselines)
		baselinesChecksums = np.asarray(baselinesChecksums).astype(np.uint32)
	else:
		baselines = None
		baselinesChecksums = None
	if len(cosmics) > 0:
		cosmics = np.asarray(cosmics)
		cosmicsChecksums = np.asarray(cosmicsChecksums).astype(np.uint32)
	else:
		cosmics = None
		cosmicsChecksums = None
	del file
	stop = time.time()
	return singles, singlesChecksums, coincidences, coincidencesChecksums, pulsers, pulsersChecksums, baselines, baselinesChecksums, cosmics, cosmicsChecksums

def prepareDataset(init, checksums):
	'''
	This function needs to prepare the dataset for writing to HDF5
	Need to sort by the event id information first
	Then calculate the fletcher32 checksums for each value
	Then do the bitshift operation on the waveform data
	'''
	start = time.time()
	#first do the sorting of the dataset
	indices = np.argsort(init['header']['eventid'])
	start2 = time.time()
	newHeaderType = []
	for element in init['header'].dtype.descr:
		newHeaderType.append(element)
	newHeaderType.append(('checksum', 'u4'))
	newHeaders = np.zeros(len(indices), dtype=np.dtype(newHeaderType))
	for name in init['header'].dtype.names:
		newHeaders[name] = np.copy(init['header'][name][indices])
	newHeaders['checksum'] = np.copy(checksums[indices])
	start3 = time.time()
	temp = bf.doAnd(init['wave'])
	waveforms = temp[indices]
	del init
	del checksums
	del indices
	return newHeaders, waveforms

def parseWaveformFiles(directory, run, subrun, headertype):
	'''
	This function parses the input waveform files and creates arrays for each waveform type. 
	It does this in a way that allows for differing waveform lengths per event type
	'''
	files = glob.glob(directory+'Run'+str(run)+'_'+str(subrun)+'RIO*.wave')
	singles = []
	singlesChecksums = []
	coincidences = []
	coincidencesChecksums = []
	pulsers = []
	pulsersChecksums = []
	baselines = []
	baselinesChecksums = []
	cosmics = []
	cosmicsChecksums = []
	startTime = time.time()
	for file in files:
		s, sChecks, c, cChecks, p, pChecks, b, bChecks, cos, cosChecks = parseFile(file, headertype)
		if s is not None:
			singles.append(s)
			singlesChecksums.append(sChecks)
		if c is not None:
			coincidences.append(c)
			coincidencesChecksums.append(cChecks)
		if p is not None:
			pulsers.append(p)
			pulsersChecksums.append(pChecks)
		if b is not None:
			baselines.append(b)
			baselinesChecksums.append(bChecks)
		if cos is not None:
			cosmics.append(cos)
			cosmicsChecksums.append(cosChecks)
	print('\t\t file reading time = ', time.time()-startTime)
	#make singular arrays for each of these
	if len(singles) > 0:
		singles = np.concatenate(singles)
		singlesChecksums = np.concatenate(singlesChecksums)
		if singles.ndim == 2:
			wavelength = singles.shape[1] - 13
			dtype = ff.headWaveType(wavelength, 'Nab', 4)
			singles = singles.view(dtype)[:,0]
			singles = prepareDataset(singles, singlesChecksums)
		else:
			singles = None
	else:
		singles = None
	if len(coincidences) > 0:
		coincidences = np.concatenate(coincidences)
		coincidencesChecksums = np.concatenate(coincidencesChecksums)
		if coincidences.ndim == 2:
			wavelength = coincidences.shape[1] - 13
			dtype = ff.headWaveType(wavelength, 'Nab', 4)
			coincidences = coincidences.view(dtype)[:,0]
			coincidences = prepareDataset(coincidences, coincidencesChecksums)
		else:
			coincidences = None
	else:
		coincidences = None
	if len(pulsers) > 0:
		pulsers = np.concatenate(pulsers)
		pulsersChecksums = np.concatenate(pulsersChecksums)
		if pulsers.ndim == 2:
			wavelength = pulsers.shape[1] - 13
			dtype = ff.headWaveType(wavelength, 'Nab', 4)
			pulsers = pulsers.view(dtype)[:,0]
			pulsers = prepareDataset(pulsers, pulsersChecksums)
		else:
			pulsers = None
	else:
		pulsers = None
	if len(baselines) > 0:
		baselines = np.concatenate(baselines)
		baselinesChecksums = np.concatenate(baselinesChecksums)
		if baselines.ndim == 2:
			wavelength = baselines.shape[1] - 13
			dtype = ff.headWaveType(wavelength, 'Nab', 4)
			baselines = baselines.view(dtype)[:,0]
			baselines = prepareDataset(baselines, baselinesChecksums)
		else:
			baselines = None
	else:
		baselines = None
	if len(cosmics) > 0:
		cosmics = np.concatenate(cosmics)
		cosmicsChecksums = np.concatenate(cosmicsChecksums)
		if cosmics.ndim == 2:
			wavelength = cosmics.shape[1] - 13
			dtype = ff.headWaveType(wavelength, 'Nab', 4)
			cosmics = cosmics.view(dtype)[:,0]
			cosmics = prepareDataset(cosmics, cosmicsChecksums)
		else:
			cosmics = None
	else:
		cosmics = None
	stopTime = time.time()
	print('\t\t reading/sorting waveform data took ', stopTime - startTime)
	return singles, coincidences, pulsers, baselines, cosmics

def replayWaveformsIntoHDF5(fileWaveforms, directory, run, subrun, compression = False, batchSize = 1024, m = 8):
	formatNumber = 4
	#open the HDF5 file first
	headertype = np.dtype(ff.headerType(fileFormat = 'Nab', formatNumber = formatNumber))
	#okay with the file opened, need to add in the waveform information
	waveforms = parseWaveformFiles(directory, run, subrun, headertype)
	startTime = time.time()
	names = ['singles', 'coincidences', 'pulsers', 'baselines', 'cosmics']
	for name, waves in zip(names, waveforms):
		print('\t \t', name)
		if waves is None:
			fileWaveforms.create_dataset('waveforms/'+name+'/headers', data = h5py.Empty(headertype))
			fileWaveforms.create_dataset('waveforms/'+name+'/waveforms', data = h5py.Empty('int16'))
		else:
			fileWaveforms.create_dataset('waveforms/'+name+'/headers', data = waves[0], fletcher32=True)
			if not compression or not __compressionAvailable:
				fileWaveforms.create_dataset('waveforms/'+name+'/waveforms', data = waves[1], fletcher32=True)
			else:
				defSize = batchSize
				if defSize > waves[1].shape[0]:
					defSize = waves[1].shape[0]
				chunks = (defSize, waves[1].shape[1])
				fileWaveforms.create_dataset('waveforms/'+name+'/waveforms', data = waves[1], fletcher32=True, chunks=chunks, compression=deltaRice.h5.H5FILTER, compression_opts=(m, waves[1].shape[1]))
	stopTime = time.time()
	print('\t\t Writing HDF5 File Took ', stopTime-startTime)
	return

def handleDamagedEvents(testfile):
	"""
	This function is designed to parse the broken event structure that has since been fixed. 
	There was a bug in the DAQ that caused the long baseline trace event type and the
	temperature recording event type to have errors when accessed through HDF5. This function
	returns an array that if saved in the place of the original event data would solve the problem
	by patching the damaged events with fake trigger information.
	
	Parameters
	----------
	testfile: HDF5 file object
		The opened input file
		Assumes the events are in the 'events' dataset
	
	Returns
	-------
	events: np.ndarray
		A numpy.ndarray object containing all of the events after any "bad" events were fixed.
		"bad" events have fake trigger information put in them now to fix the file reading issues
	"""
	outEvents = []
	for i in range(len(testfile['events'])):
		try:
			outEvents.append(testfile['events'][i])
		except:
			print(i)
			#if that fails, the issue is that the triggers messed it up
			tempEvent = np.zeros(1, dtype=dtype)
			for field in list(dtype.fields)[:-2]:
				tempEvent[field] = testfile['events'][field][i]
			#now for the trigger
			tempEvent['number of triggers'] = 1
			fakeTrigger = np.zeros(1, dtype=triggertype)
			fakeTrigger['timestamp'] = testfile['events']['baseline timestamp'][i]
			tempEvent['triggers'] = fakeTrigger
			outEvents.append(tempEvent[0])
	outEvents = np.array(outEvents, dtype=dtype)
	return outEvents

def readAllTriggersEventsSafely(inputFileLocation, run, subrun, triggers=True):
	if triggers:
		#then we are managing the trigger file this time
		triggerType = ff.triggerType(version='reading')
		file = np.fromfile(inputFileLocation+'Run'+str(run)+'_'+str(subrun)+'.trig', dtype=triggerType)
		return file
	else:
		#in this case we're handling the events
		eventType = ff.eventType(version=1)
		file = np.fromfile(inputFileLocation+'Run'+str(run)+'_'+str(subrun)+'.evnt', dtype=eventType)
		return file

def convertFileFormatFullDAQ(inputFileLocation, outputFileLocation, run, subrun, compression = False, batchSize = 1024, m = 8, runLog="/mnt/NabRaid0/Nab/RunLog.csv", runLogSep=','):
	"""
	This function is effectively the first step in the replay process. 
	It reads in the initial DAQ output file and makes some simple modifications to how the data is stored before outputting a new version of the file.
	
	These modifications are focused around how the waveforms are stored but some changes are done to the parameter information to make it easier to handle.
	
	Parameters
	------
	inputFileName: string
		the name and location of the input file
	
	outputFileName: string
		the name and location of the output file
	
	compression: bool (defaults to False)
		If this is set to True, and if the nabCompression.h5 libraries are available, then compression will be enabled for waveform data.
		Otherwise if either of those conditions aren't met, the waveform data will not be compressed.
	
	Returns
	-------
	None
	"""
	startTime = time.time()
	inputFileName = inputFileLocation + 'Run'+str(run)+'_'+str(subrun)+'.h5'
	outputFileWavesName = outputFileLocation + 'Run'+str(run)+'_'+str(subrun)+'.h5'
	#outputFileProcName = outputFileLocation + 'procRun'+str(run)+'_'+str(subrun)+'.h5'
	testIn = h5py.File(inputFileName, 'r')
	testOutWaves = h5py.File(outputFileWavesName, 'w')
	try:
		print('Converting File')
		#first copy the FPGA temperature data over
		print('\t Starting FPGA Temperatures')
		if testIn['FPGATemperatures'].shape != 0: #if it's not empty, make this
			testOutWaves.create_dataset('FPGATemperatures', data = testIn['FPGATemperatures'], fletcher32=True)
		gc.collect()
		#now copy the parameter information over
		print('\t Starting Parameters')
		for key in testIn['Parameters'].keys():
			gc.collect()
			datasetName = 'Parameters/'+key
			#print(f'\t \t {datasetName}')
			'''
			if key == 'RunSettings': #unpack these into an easier to parse form
				data = testIn[datasetName]
				for f in data.dtype.names:
					subname = datasetName + '/'+f
					testOutWaves[subname] = testIn[datasetName][()][f]
				del data
			elif key == 'HardwareConfiguration' or key == 'RunTime' or key == 'RunMetadata' or key == 'DAQStability': #unpack these into an easier to parse form
				group = testIn[datasetName]
				for k in group.keys():
					subname = datasetName + '/'+k
					try:
						testOutWaves[subname] = group[k][()]
					except:
						testOutWaves[subname] = 0
				del group
			else: #other ones are just fine how they are
				testOutWaves[datasetName] = testIn[datasetName][()]
			'''
			try: # Attempt to just write out the data into the output file
				testOutWaves[datasetName] = testIn[datasetName][()]
			except TypeError: # If that failed, it's probably a group
				group = testIn[datasetName]
				for k in group.keys():
					subname = datasetName + '/'+k
					try:
						testOutWaves[subname] = group[k][()]
					except:
						testOutWaves[subname] = 0
				del group
		print('\t Updating Run Log')
		if updateRunLog(run, testIn['Parameters/RunDescription'][()].decode(), testIn['Parameters/RunTime'], testIn['Parameters/RunMetadata'], testIn['Parameters/Errors'][()], testIn['Parameters/DAQStability'], logFile=runLog, sep=runLogSep) == -1:
		#if not updateRunLog(runLog, run, testIn['Parameters/RunDescription'][()].decode(), testIn['Parameters/RunTime/StartTime'][()][0]):
			print(f"Couldn't update log for run {run}.")
		print('\t Starting Triggers')
		#copy over the trigger information
		testOutWaves.create_dataset('triggers', data = readAllTriggersEventsSafely(inputFileLocation, run, subrun, triggers=True), fletcher32=True)
		gc.collect()
		#copy the event information over
		print('\t Starting Events')
		testOutWaves.create_dataset('events', data = readAllTriggersEventsSafely(inputFileLocation, run, subrun, triggers=False), fletcher32=True)
		gc.collect()
		testIn.close()
		print('\t Starting to parse the waveform data')
		replayWaveformsIntoHDF5(testOutWaves, inputFileLocation, run, subrun, compression = compression, batchSize = batchSize, m = m)
		testOutWaves.close()
		gc.collect()
		stopTime = time.time()
		print('\t time taken', stopTime - startTime)
		return 0
	except Exception as e:
		print('error during execution')
		print(e)
		testIn.close()
		testOutWaves.close()
		return -1
	return 0

def createPreprocessedFile(inputFileLocation, outputFileLocation, run, subrun, rise=1250, top = 100, tau = 1250, pretrigger = 1000, useGPU = False):
	"""
	This function is effectively the first step in the replay process. 
	It reads in the initial DAQ output file and makes some simple modifications to how the data is stored before outputting a new version of the file.
	
	These modifications are focused around how the waveforms are stored but some changes are done to the parameter information to make it easier to handle.
	
	Parameters
	------
	inputFileName: string
		the name and location of the input file
	
	outputFileName: string
		the name and location of the output file
	
	rise: all of these are for the trapezoidal filter function
	top
	tau
	pretrigger
	Returns
	-------
	None
	"""
	inputFileName = inputFileLocation + 'Run'+str(run)+'_'+str(subrun)+'.h5'
	#outputFileWavesName = outputFileLocation + 'Run'+str(run)+'_'+str(subrun)+'.h5'
	outputFileProcName = outputFileLocation + 'procRun'+str(run)+'_'+str(subrun)+'.h5'
	testIn = h5py.File(inputFileName, 'r')
	#testOutWaves = h5py.File(outputFileWavesName, 'w')
	testOutProc = h5py.File(outputFileProcName, 'w')
	try:
		print('Preprocessing File')
		#first copy the FPGA temperature data over
		print('\t Starting FPGA Temperatures')
		if testIn['FPGATemperatures'].shape != 0: #if it's not empty, make this
			testOutProc.create_dataset('FPGATemperatures', data = testIn['FPGATemperatures'], fletcher32=True)
		gc.collect()
		#now copy the parameter information over
		print('\t Starting Parameters')
		for key in testIn['Parameters'].keys():
			gc.collect()
			datasetName = 'Parameters/'+key
			if key == 'RunSettings' or key == 'HardwareConfiguration' or key=='RunTime' or key == 'RunMetadata': #unpack these into an easier to parse form
				group = testIn[datasetName]
				for k in group.keys():
					subname = datasetName + '/'+k
					testOutProc[subname] = group[k][()]
				del group
			else: #other ones are just fine how they are
				testOutProc[datasetName] = testIn[datasetName][()]
		#copy over the trigger information
		testOutProc.create_dataset('triggers', data = testIn['triggers'], fletcher32=True)
		gc.collect()
		#copy the event information over
		print('\t Starting Events')
		try:
			testOutProc.create_dataset('events', data = testIn['events'])
			gc.collect()
		except:
			print('\t Event processing failed. Moving on!')
		print('\t Starting to process the waveform data')
		try:
			for key in testIn['waveforms'].keys():
				print(f"\t\t{key}")
				if key == 'baselines':
					continue
				preprocessWaveformData(testIn, testOutProc, key, rise=rise, top=top, tau=tau, pretrigger=pretrigger, useGPU=useGPU)
		except Exception as e:
			print('error during preprocessing')
			print(e)
			testIn.close()
			testOutProc.close()
			return -1
		testIn.close()
		testOutProc.close()
		gc.collect()
		return 0
	except Exception as e:
		print('error during execution')
		print(e)
		testIn.close()
		testOutProc.close()
		return -1
	return 0
	
def preprocessWaveformData(h5FileIn, h5FileOut, waveType, rise=1250, top=100, tau=1250, pretrigger=1000, useGPU=False):
	"""
	Applies the trap filter to a waveform dataset.
	"""
	try:
		h5FileOut.create_dataset(f'waveforms/{waveType}/headers', data = h5FileIn[f'waveforms/{waveType}/headers'][()], fletcher32=True)
		data = h5FileIn[f'waveforms/{waveType}/waveforms'][()]
		en, tim = bf.applyTrapFilter(data, rise, top, tau, useGPU=useGPU)
		h5FileOut.create_dataset(f'waveforms/{waveType}/energy', data = en, fletcher32=True)
		h5FileOut.create_dataset(f'waveforms/{waveType}/timing', data = tim, fletcher32=True)
	except:
		print('\terror while applying trap filter or creating dataset')
	finally:
		return

def verifyDataIntegrity(initialFile, finalFile):
	"""
	This function verifies that all the data was preserved during the replay process.
	It opens up the original file and the new file and confirms that all data matches
	
	Parameters
	------
	initialFile: string
		the name and location of the input file
	
	finalFile: string
		the name and location of the output file
	
	Returns
	-------
	result: int
		If result = 0: file matches and data is okay
		If result < 0: file doesn't match
			-1: FPGA temperature data doesn't match
			-2: Run Settings mismatch
			-3: Hardware Configuration Mismatch
			-4: Generical configuration parameter mismatch
			-5: Trigger data mismatch
			-6: Event data mismatch
			-7: Waveform data headers mismatch
			-8: Waveform data waveforms mismatch
			-9: Error during data quality check, assume problem
	"""
	testIn = h5py.File(initialFile, 'r')
	testOut = h5py.File(finalFile, 'r')
	try:
		#first copy the FPGA temperature data over
		if testIn['FPGATemperatures'].shape != 0: #if it's not empty, check this
			inp = testIn['FPGATemperatures'][()]
			out = testOut['FPGATemperatures'][()]
			if not np.array_equal(inp, out):
				return -1
			del inp
			del out
			gc.collect()
		#now copy the parameter information over
		for key in testIn['Parameters'].keys():
			datasetName = 'Parameters/'+key
			if key == 'RunSettings': #unpack these into an easier to parse form
				data = testIn[datasetName]
				for f in data.dtype.names:
					subname = datasetName + '/'+f
					if not np.array_equal(testOut[subname][()], testIn[datasetName][()][f]):
						return -2
			elif key == 'HardwareConfiguration': #unpack these into an easier to parse form
				group = testIn[datasetName]
				for k in group.keys():
					subname = datasetName + '/'+k
					if not np.array_equal(testOut[subname][()], group[k][()]):
						return -3
			else: #other ones are just fine how they are
				if not np.array_equal(testOut[datasetName][()], testIn[datasetName][()]):
					return -4
		gc.collect()
		#copy over the trigger information
		inp = testOut['triggers'][()]
		out = testIn['triggers'][()]
		if not np.array_equal(inp, out):
			return -5
		del inp
		del out
		gc.collect()
		#copy the event information over
		inp = testOut['events'][()]
		out = testIn['events'][()]
		if not np.array_equal(inp, out):
			return -6
		del inp
		del out
		gc.collect()
		#now read in the waveform data and verify the checksums all match what they should
		for key in testOut['waveforms']:
			headers = testOut['waveforms/'+str(key)+'/headers'][()]
			waveforms = testOut['waveforms/'+str(key)+'/waveforms'][()]
			#check if these datasets are empty or not
			if headers.shape is not None:
				#if they have stuff, calculate the checksums and verify it all matches
				checksumsInit = headers['checksum']
				calculated = np.apply_along_axis(bf.Fletcher32, 1, waveforms.view(np.uint16))
				equal = np.array_equal(calculated, checksumsInit)
				if not equal:
					return -7
		testIn.close()
		testOut.close()
		del testIn
		del testOut
		gc.collect()
		return 0
	except Exception as e:
		print('Error occured during Data Integrity Check')
		print(e)
		testIn.close()
		testOut.close()
		del testIn
		del testOut
		gc.collect()
		return -9
	gc.collect()
	return 0

def findHDF5FilesToOperateOn(filedir, split=None):
	"""
	This script looks for a .fin file
	These are indicators from the DAQ that a new file should be analyzed
	Once one is found, a hdf5 file with the appropriate name is identified
	and it's existence is verified.
	
	Those that pass all these checks are ready for analysis.
	"""
	files = os.listdir(filedir)
	if len(files) == 0: #no files found
		return None
	else:
		hdf5Files = []
		for file in files:
			name, ext = os.path.splitext(file)
			if ext == '.end':
				hdf5name = os.path.join(filedir, name) +'.h5'
				if os.path.exists(hdf5name):
					hdf5Files.append(hdf5name)
				else:
					print(f'{name}{ext} HDF5 File not found!!!')
					print('Possible Dangling .end file')
		hdf5Files = sorted(hdf5Files, key=os.path.getmtime)
		if split == 'even':
			return
		else:
			return hdf5Files
		
def grabSpecificRun(runNum, direct):
	files = glob.glob(f"{direct}Run{runNum}_*.h5")
	return files

def deleteFinFile(file):
	"""
	This function looks for the .end file that indicated that we should do analysis
	and deletes it. This is only called after the analysis is done.
	The expected name passed to this is the HDF5 file name
	"""
	finName = os.path.splitext(file)[0]
	finName+='.end'
	os.remove(finName)
	return
	
def moveFinFile(file,out):
	"""
	This function looks for the .end file that indicated that we should do analysis
	and moves it. This is only called after the analysis is done.
	The expected name passed to this is the HDF5 file name and the output directory
	"""
	finName = os.path.splitext(file)[0]
	finName+='.end'
	foutName = out + finName.split('/')[-1]
	shutil.move(finName,foutName)
	return

def checkForStopSignal(directory):
	"""
	Checks for a file signal to stop the replay code
	
	Parameters
	----------
	directory: string
		The location to search for the stop signal in
	
	Returns
	-------
	result: boolean
		If true, then the code should continue running
		If false, then the replay code should stop because stop.stop has been found
	"""
	files = os.listdir(directory)
	if 'stop.stop' in files:
		return False
	else:
		return True

def __grabRunSubRunNumbers(filename):
	run = None
	subrun = None
	try:
		withoutRun = filename.split('Run')[1]
		withoutExtension = withoutRun.split('.h5')[0]
		run, subrun = withoutExtension.split('_')
		run = int(run)
		subrun = int(subrun)
	except:
		None
	return run, subrun
	
def grabRunNumsFromLog(logFile="/mnt/NabRaid0/Nab/RunLog.csv"):
	# First see if the file exists
	if not os.path.isfile(logFile):
		# In this case, it's obviously not a duplicate 
		return []
	else:
		# If it does exist, try to read the run numbers.
		try:
			runs = list(pd.read_csv(logFile, usecols=['RunNumber'], on_bad_lines='skip')['RunNumber'])
		except Exception as e:
			print("Couldn't read in run numbers.")
			print(e)
			return -1
		else:
			return runs

def packageRunDataForLogging(runDesc, runTime, runMeta, runErr, stab):
	'''
	Takes the run description, run times, and run metadata and packs it all into a dictionary.
	Returns an empty dictionary if it encounters an error.
	'''
	try:
		# Start with the run description
		d = {"RunDescription": runDesc}
		# Now go through each run time key
		for t in runTime.keys():
			d.update({t: runTime[t][()][0]})
		# And finally the run metadata
		for m in runMeta.keys():
			mDat = runMeta[m][()]
			# Throw this part in for strings.
			if type(mDat) == bytes:
				mDat = mDat.decode()
			d.update({m: mDat})
		# Add any error codes
		errStat = runErr[0]
		errCode = runErr[1]
		err = runErr[2].decode().replace("\n", " ").replace("\t", " ").replace("\r", " ") # Replace returns and tabs with a space
		# This is a special case where the sub run ended okay, so we'll say there was no error.
		if errCode == 5004:
			errStat = 0
			errCode = 0
			err = "No error."
		elif not err:
			err = "No error."
		d.update({"ErrorStatus": errStat})
		# And the source
		d.update({"ErrorCode": errCode})
		# And the description
		d.update({"ErrorDescription": err})
		# Now the stability parameters
		for s in stab.keys():
			try:
				sDat = stab[s][()]
			except:
				sDat = 0
			d.update({s: sDat})
	except Exception as e:
		print("Failed to package run data.")
		print(e)
		return {}
	return d
	
def appendMetaDF(df, runNum, runDesc, runTime, runMeta, runErr, stab):
	'''
	Appends a metadata dataframe with a new run.
	Creates a new one if 'None' is passed into df.
	Returns the new dataframe.
	Returns an empty dataframe if an error was encountered.
	'''
	try:
		d = packageRunDataForLogging(runDesc, runTime, runMeta, runErr, stab)
		if not d:
			return pd.DataFrame()
		new = pd.DataFrame(d, index=[runNum])
		if type(df) == 'NoneType':
			return new
		else:
			return pd.concat((df, new))
	except Exception as e:
		print("Couldn't concatenate dataframes.")
		print(e)
		return pd.DataFrame()
	
def writeToLog(df, logFile="/mnt/NabRaid0/Nab/RunLog.csv", sep=','):
	'''
	Writes the dataframe to a CSV file.
	Includes headers only if the file hasn't been created yet.
	'''
	try:
		# If the file is new, include the column names
		if not os.path.isfile(logFile):
			df.to_csv(logFile, sep=sep, index_label='Run Number')
		# Otherwise, don't include them.
		else:
			df.to_csv(logFile, sep=sep, mode='a', header=False)
	except Exception as e:
		print("Couldn't write to log.")
		print(e)
		return -1

def updateRunLog(runNum, runDesc, runTime, runMeta, runErr, stability, logFile="/mnt/NabRaid0/Nab/RunLog.csv", sep=','):
	'''
	Updates a run log CSV file with the run number, description, times, metadata, and any errors from the run.
	Given a file, read in the relevant material and append that to the run log CSV file.
	Appends all runs in the list into one dataframe before dumping to the CSV file.
	'''
	df = None
	# Read in the existing run numbers
	runs = grabRunNumsFromLog(logFile=logFile)
	if runs == -1:
		return -1
	err = True
	if runErr[1] == 0 or runErr[1] == 5004:
		err = False
	# Check if this is a duplicate
	# Skip it ONLY if there was no error
	if runNum in runs and not err:
		return 0
	else:
		# Reads in metadata and appends it to a dataframe (or creates a new one).
		appRes = appendMetaDF(df, runNum, runDesc, runTime, runMeta, runErr, stability)
		if appRes.empty:
			print("Failed trying to append dataframe.")
			return -1
		else:
			df = appRes
		runs.append(runNum)
	if writeToLog(df, logFile=logFile, sep=sep) == -1:
		return -1

def updateRunLogOLD(runLog, runNum, runDesc, startTime):
	"""
	Updates a run log text file with the run number, run description, and run start time.
	"""
	# First check if this run has already been done.
	runs = [int(d.split('\t')[0].split('_')[0]) for d in open(runLog,'r').readlines()]
	if runNum not in runs:
		with open(runLog,'a') as f:
			try:
				f.write(f"{runNum}\t{runDesc}\t{startTime}\n")
			except:
				return False
			else:
				return True
	else:
		return True

		
def __childProcess(file, inputDir = "/mnt/CacheSSD/", outputDir = '/mnt/NabRaid0/Nab/ReplayOutput/', badFolder = '/mnt/NabRaid0/Nab/ProblemFiles/', saveOrigFolder = '/mnt/NabRaid0/Nab/PreReplay/', saveOrig = False, queryTime = 5, compression = False, batchSize = 1024, m = 8, runLog="/mnt/NabRaid0/Nab/RunLog.csv", runLogSep=','):
	print('starting replay script on file: ', file)
	basename = os.path.basename(file)
	outputName = os.path.join(outputDir, basename)
	run, subrun = __grabRunSubRunNumbers(basename)
	try:
		if run != None:
			conversionRes = convertFileFormatFullDAQ(inputDir, outputDir, run, subrun, compression = compression, batchSize = batchSize, m = m, runLog=runLog, runLogSep=runLogSep)
			fileList = []
			fileList.append(inputDir+'Run'+str(run)+'_'+str(subrun)+'.h5')
			fileList.append(inputDir+'Run'+str(run)+'_'+str(subrun)+'.trig')
			fileList.append(inputDir+'Run'+str(run)+'_'+str(subrun)+'.evnt')
			otherFiles = glob.glob(inputDir+'Run'+str(run)+'_'+str(subrun)+'RIO*.wave')
			for f in otherFiles:
				fileList.append(f)
			gc.collect()
			if conversionRes != 0:
				print("\t replay failed :( ")
				for f in fileList:
					basename = os.path.basename(f)
					shutil.move(f, badFolder+basename)
				#this file failed, move it to the bad folder
				deleteFinFile(file)
			else:
				for f in fileList:
					basename = os.path.basename(f)
					if saveOrig:
						shutil.move(f, saveOrigFolder+basename)
					else:
						os.remove(f)
				deleteFinFile(file)
				#moveFinFile(file, outputDir)
			return 0
		else:
			print('\t Run'+str(run)+'_'+str(subrun)+'.stop exists with no corresponding .h5 file')
			print('\t ignoring this run and moving on')
			return 1
	except Exception as e:
		print('Error occured on child process\n')
		print(e)
		print('moving files to '+badFolder)
		fileList = []
		fileList.append(inputDir+'Run'+str(run)+'_'+str(subrun)+'.h5')
		fileList.append(inputDir+'Run'+str(run)+'_'+str(subrun)+'.trig')
		fileList.append(inputDir+'Run'+str(run)+'_'+str(subrun)+'.evnt')
		otherFiles = glob.glob(inputDir+'Run'+str(run)+'_'+str(subrun)+'RIO*.wave')
		for f in otherFiles:
			fileList.append(f)
		for f in fileList:
			basename = os.path.basename(f)
			shutil.move(f, badFolder+basename)
		deleteFinFile(file)
		return 2
	return 0		

def __preProcChildProcess(file, inputFileLocation, outputFileLocation, rise=1250, top = 100, tau = 1250, pretrigger = 1000, useGPU = False):
	try:
		print('starting replay script on file: ', file)
		basename = os.path.basename(file)
		outputName = os.path.join(outputFileLocation, basename)
		run, subrun = __grabRunSubRunNumbers(basename)
		if run != None:
			preProcRes = createPreprocessedFile(inputFileLocation, outputFileLocation, run, subrun, rise=rise, top=top, tau=tau, pretrigger=pretrigger, useGPU=useGPU)
			gc.collect()
			if preProcRes != 0:
				print("\t preprocessing failed :( ")
				try:
					deleteFinFile(file)
				except FileNotFoundError:
					print(".end file already gone.")
			try:
				deleteFinFile(file)
			except FileNotFoundError:
				print(".end file already gone.")
			return 0
		else:
			print('\t Run'+str(run)+'_'+str(subrun)+'.stop exists with no corresponding .h5 file')
			print('\t ignoring this run and moving on')
			deleteFinFile(file)
			return 1
	except Exception as e:
		print('Error occured on child process\n')
		print(e)
		deleteFinFile(file)
		return 2
	return 0
		
def replayScript(inputDir = "/mnt/CacheSSD/", outputDir = '/mnt/NabRaid0/Nab/ReplayOutput/', badFolder = '/mnt/NabRaid0/Nab/ProblemFiles/', saveOrigFolder = '/mnt/NabRaid0/Nab/PreReplay/', saveOrig = True, queryTime = 5, compression = False, batchSize = 1024, m = 8, runLog="/mnt/NabRaid0/Nab/RunLog.csv", numProcs = 1, useGPU = False, preProcMode = False, specificRun=None, runLogSep=','):
	"""
	This is the actual replay script. It's job is to identify files that have recently arrived from the DAQ system and process them.
	
	It will replay the files into a format that is easily handled by pyNab and offline analysis in general.
	Eventually this will have additional post-processing functionality.
	For now, it's just converting file formats.
	
	Parameters
	----------
	inputDir: string
		Defaults to : /mnt/CacheSSD/
		The location to look for incoming files
	
	outputDir: string
		Defaults to /mnt/raid/Nab/ORNL
		The location to output the files to
	
	badFolder: string
		The location where problematic files that broke the replay script should be moved to
		Any files that fail to replay, or replayed files that fail to verify, are moved here
	
	queryTime: int
		Defaults to 5
		The time to wait to look for new files if no files were found
	
	compression: bool
		Defaults to False
		Enables compression via deltaRice
	
	batchSize: 1024
		The number of waveforms per chunk in the output dataset
	
	m: 8
		Golomb coding parameter used in the deltaRice function
	
	runLogF: '/mnt/NabRaid0/Nab/'
		The location of the output run log
	
	numProcs: 1
		How many files to replay simultaneously
		Limit this to below 4 at most I think
	preProcMode: False
		Flag indicating whether or not the script should run in pre-processing mode.
		Enable this to perform preprocessing instead of conventional replaying.
	specificRun: None
		Can specify a particular run to replay. Will ignore the .end file search and go straight to replaying.
	
	Returns
	-------
	None
	"""
	print('Replay Code Running')
	if specificRun:
		if preProcMode:
			try:
				toOperateOn = grabSpecificRun(specificRun, inputDir)
				for file in toOperateOn:
					if not checkForStopSignal(inputDir):
						print('Stop Signal Received')
						#if the code stopped due to a stop.stop file appearing, delete it
						os.remove(os.path.join(inputDir, 'stop.stop'))
						return
					with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
						preProcResult = __preProcChildProcess(file, inputDir, outputDir, rise=1250, top = 100, tau = 1250, pretrigger = 1000, useGPU = useGPU)
			except:
				print(f'failed to preprocess run {specificRun}')
				return
			else:
				return
		else:
			try:
				toOperateOn = grabSpecificRun(specificRun, inputDir)
				for file in toOperateOn:
					if not checkForStopSignal(inputDir):
						print('Stop Signal Received')
						#if the code stopped due to a stop.stop file appearing, delete it
						os.remove(os.path.join(inputDir, 'stop.stop'))
						return
					with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
						result = __childProcess(file, inputDir=inputDir, outputDir=outputDir, badFolder=badFolder, saveOrigFolder=saveOrigFolder, saveOrig=saveOrig, queryTime=queryTime, compression=compression, batchSize=batchSize, m=m, runLog=runLog, runLogSep=runLogSep)
			except:
				print(f'failed to replay run {specificRun}')
				return
			else:
				return
	print('Looking for Files in Directory: ' + inputDir)
	go = True
	#pool = multiprocessing.Pool(numProcs)
	while go:
		go = checkForStopSignal(inputDir)
		if not go:
			print('Stop Signal Received')
			#if the code stopped due to a stop.stop file appearing, delete it
			os.remove(os.path.join(inputDir, 'stop.stop'))
			return
		toOperateOn = findHDF5FilesToOperateOn(inputDir)
		if toOperateOn is not None:
			if isinstance(toOperateOn, int): #in this case it's an error code
				print('error found during findHDF5FilesToOperateOn')
				print('System stopping')
				go = False
			elif isinstance(toOperateOn, list):
				#in this case we found some files and want to analyze them
				for file in toOperateOn:
					#get the original file name without directory
					result = None
					if not checkForStopSignal(inputDir):
						#if the code stopped due to a stop.stop file appearing, delete it
						os.remove(os.path.join(inputDir, 'stop.stop'))
						go = False
						return
					if preProcMode:
						#if os.path.isfile(os.path.join(preProcIn,file.split('/')[1])):
						with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
							preProcResult = __preProcChildProcess(file, inputDir, outputDir, rise=1250, top = 100, tau = 1250, pretrigger = 1000, useGPU = useGPU)
					else:
						with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
							result = __childProcess(file, inputDir=inputDir, outputDir=outputDir, badFolder=badFolder, queryTime=queryTime, compression=compression, batchSize=batchSize, m=m, runLog=runLog, saveOrig=saveOrig, saveOrigFolder=saveOrigFolder, runLogSep=runLogSep)
		else:
			time.sleep(queryTime)
	if not checkForStopSignal(inputDir): 
		#if the code stopped due to a stop.stop file appearing, delete it
		os.remove(os.path.join(inputDir, 'stop.stop'))
	return
