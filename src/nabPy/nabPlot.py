import numpy as np #the usual
import matplotlib.pyplot as plt #import for plotting
import matplotlib.patches as patches #import for pixel maps
import matplotlib.colors as colors #coloration
import matplotlib.cm as cmx #not sure
import string


class detectorFigure:
    def __init__(self, size=1, alpha=1, cmap='cividis', logNorm=False, numDet=1):
        self.size = size
        self.alpha = alpha
        self.cmap = cmap
        self.logNorm = logNorm
        self.numDet = numDet
        self.fig = None
        self.ax = None
        self.cbar = None
        if self.numDet != 1:
            print(f'Multiple detectors not currently supported... defaulting to 1.')
            self.numDet = 1
        self.__initGeometry()
    def __initGeometry(self):
        self.__vertOffset = self.size * np.sqrt(3)
        self.__horOffset = self.size * 1.5
        self.__colEnd = np.array([7, 15, 24, 34, 45, 57, 70, 82, 93, 103, 112, 120, 127])
        self.__colStart = np.array([1, 8, 16, 25, 35, 46, 58, 71, 83, 94, 104, 113, 121])
        self.__colLen = np.array(self.__colEnd) - np.array(self.__colStart) + 1
        self.__numCol = len(self.__colEnd)
        self.__ranges = [np.arange(i,j+1) for i,j in zip(self.__colStart, self.__colEnd)]
    def _generateCoordinates(self, values):
        '''
        Given input values, generates coordinates of each item on the detector geometry.
        Returns a 2D array with the list of horizontal and vertical positions.
        '''
        self.values = values
        self.__valRange = np.arange(1,len(self.values)+1)
        self.__cols = np.array([np.where([p in column for column in self.__ranges])[0][0] for p in self.__valRange])
        self.__numInCol = self.__valRange - self.__colStart[self.__cols]
        self.__horPositions = (self.__cols - self.__numCol/2) * self.__horOffset
        self.__verPositions = (self.__colLen[self.__cols]-1)*self.__vertOffset/2 - self.__vertOffset*self.__numInCol
        return np.array((self.__horPositions,self.__verPositions)).T
    def createFigure(self, values, forceMin=None, forceMax=None):
        '''
        Given input values, creates the figure object.
        Closes the figure after creating it. To actually plot it, run showFigure function or just call the figure variable if you're using an instance like Jupyter.
        *For now, I'm coding this assuming one detector... should fix that in the future.*
        '''
        self.forceMin = forceMin
        self.forceMax = forceMax
        if self.fig is None:
            self.fig, self.ax = plt.subplots(self.numDet, figsize=(self.size * 7 + self.size, self.size * 7), constrained_layout = True)
        # Set sizes
        self.ax.set_xlim(-self.size * 13, self.size * 13)
        self.ax.set_ylim(-self.size * 13, self.size * 13)
        # Set minimum and maximum values
        self._minval = np.min(values)
        self._maxval = np.max(values)
        if self.forceMin is not None:
            self._minval = self.forceMin
        elif self.logNorm:
            if self._minval <= 0:
                self._minval = 0.01
        if self.forceMax is not None:
            self._maxval = self.forceMax
        # Set the color map
        self._cm = plt.get_cmap(self.cmap)
        if self.logNorm:
            self._cNorm = colors.LogNorm(self._minval, self._maxval)
        else:
            self._cNorm = colors.Normalize(self._minval, self._maxval)
        self._scalarMap = cmx.ScalarMappable(norm=self._cNorm, cmap=self._cm)
        self._fcolors = self._scalarMap.to_rgba(values)
        # Set the geometry
        self._coordinates = self._generateCoordinates(values)
        # Create the hexagonal pixels
        self._hexes = np.array([
            patches.RegularPolygon((c), numVertices=6, radius=self.size, alpha=self.alpha, facecolor=fc, edgecolor='black', orientation=np.pi/2)
            for c,fc in zip(self._coordinates,self._fcolors)
        ])
        # Add them to the axis
        for h in self._hexes:
            self.ax.add_patch(h)
        if self.cbar is None:
            self.cbar = self.fig.colorbar(self._scalarMap, ax=self.ax)
        else:
            self.cbar.update_normal(self._scalarMap)
        plt.close(self.fig)
        return self.fig, self.ax, self.cbar    
    def setPixelLabels(self, ax, labels):
        '''
        Sets the labels on pixels.
        Need to pass in the axis object.
        '''
        for i in range(len(labels)):
            ax.text(self.__horPositions[i]-self.size/2, self.__verPositions[i], str(labels[i]), ma='center', va='center')
    def showFigure(self, figure):
        '''
        Intended to take a closed figure and plot it.
        Basically, use this in place of plt.show() when plotting a detector map.
        '''
        self._dummy = plt.figure()
        self._newManager = self._dummy.canvas.manager
        self._newManager.canvas.figure = figure
        figure.set_canvas(self._newManager.canvas)
    def setValueLabels(self, ax, rounding=3):
        '''
        Quick way to set the labels as the values.
        User can easily do this themselves (and with more freedom).
        '''
        if rounding:
            self._vals = np.array([round(i,rounding) for i in self.values])
        else:
            self._vals = self.values
        self.setPixelLabels(ax, np.asarray(self._vals, dtype=str))
    def setColorBarLabel(self, cbar, ax, labels):
        '''
        Set the labels on the color bar.
        Requires the color bar and axis objects as well as the labels (needs to map to the number of colors).
        '''
        cbar.ax.get_yaxis().set_ticks([])
        for j, lab in enumerate(labels):
            cbar.ax.text(0, j+self._minval, lab, ha='right', va='center')
        cbar.ax.get_yaxis().labelpad = 15
    def resetColorMap(self, cmap, logNorm=False):
        '''
        Returns the original figure but with the new color map and log norm setting.
        Alternative (and probably simpler) way to do this is to just start the process over but with a new color map.
        '''
        self.cmap = cmap
        self.logNorm = logNorm
        return self.createFigure(self, self.values, forceMin=self.forceMin, forceMax=self.forceMax)
    def resetAlpha(self, alpha):
        '''
        Returns the original figure but with the new alpha setting.
        Alternative (and probably simpler) way to do this is to just start the process over but with a new alpha.
        '''
        self.alpha = alpha
        return self.createFigure(self, self.values, forceMin=self.forceMin, forceMax=self.forceMax)    
    def resetSize(self, size):
        '''
        Returns the original figure but with the new size setting.
        Alternative (and probably simpler) way to do this is to just start the process over but with a new size.
        Note that this also re-initializes the geometry!
        '''
        self.size = size
        self._initGeometry()
        return self.createFigure(self, self.values, forceMin=self.forceMin, forceMax=self.forceMax)
    	
def returnPreampLabels(BCtoPixelMap):
    '''
    Given the input board-channel to pixel map, generates the (pixel-ordered) list of corresponding preamp labels.
    NOTE: assumes preamps are plugged sequentially into the DAQ!
        If this is not the case, it may be wise for the user to generate them themselves using similar logic.
    '''
    preampLabels = np.array([[f'{i.capitalize()}{j}' for j in range(1,7)] for i in list(string.ascii_lowercase)[:-4]])
    toDelete = np.concatenate((preampLabels[[2,10,13,-1]][:,5].flatten(),np.array(['V3']))) # Get rid of these
    preampLabels = preampLabels.flatten() # Flatten out the array
    toDeleteMask = ~np.isin(preampLabels, toDelete) # Mark which ones are not going to be deleted
    preampLabels = preampLabels[toDeleteMask] # Keep the ones we want
    sort = np.zeros(127, dtype=object)
    for i,j in BCtoPixelMap[:-1]:
        sort[j-1] = preampLabels[i]
    return sort

def plotOneDetector(values, numDet = 1, cmap = 'cividis', size = 3, showNum = True, showVal = True, alpha=1, rounding = None, title = None, norm = None, forceMin = None, forceMax = None, labels=None, filename = None, saveDontShow = False): #this is a simple function that plots values over each pixel
	fig, ax = plt.subplots(1, figsize=(size * 7 + size, size * 7), constrained_layout = True)
	ax.set_xlim(-size * 13, size * 13)
	ax.set_ylim(-size * 13, size * 13)
	cm = plt.get_cmap(cmap)
	cNorm = None
	minval = np.min(values)
	maxval = np.max(values)
	if forceMin is not None:
		minval = forceMin
	elif norm == 'log':
		if minval <= 0:
			minval = 0.01
	if forceMax is not None:
		maxval = forceMax
	if norm is None:
		cNorm = colors.Normalize(minval, maxval)
	elif norm == 'log':
		cNorm = colors.LogNorm(minval, maxval)
	else:
		print('unrecognized normalization option: needs to be log or not set')
		return
	scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
	vertOffset = size * np.sqrt(3)
	horOffset = size * 1.5
	colEnd = [7, 15, 24, 34, 45, 57, 70, 82, 93, 103, 112, 120, 127]
	colStart = [1, 8, 16, 25, 35, 46, 58, 71, 83, 94, 104, 113, 121]
	colLen = list(np.array(colEnd) - np.array(colStart) + 1)
	numCol = len(colEnd)
	for pixel in range(1, len(values)+1):
		col = 0
		for j in range(len(colEnd)):
			if pixel >= colStart[j] and pixel <= colEnd[j]:
				col = j
		numInCol = pixel - colStart[col] #number in the column from the top of the column
		horPosition = (col - numCol/2)*horOffset
		topOfCol = colLen[col]/2*vertOffset - vertOffset/2
		verPosition = topOfCol - vertOffset*numInCol
		hex = patches.RegularPolygon((horPosition, verPosition), numVertices=6, radius=size, facecolor=scalarMap.to_rgba(values[pixel-1]), orientation=np.pi/2, alpha = alpha, edgecolor='black')
		ax.add_patch(hex)
		txt = ''
		if showNum == True:
			txt += str(pixel)
		if labels is not None:
			if txt != '':
				txt += '\n'
			txt += str(labels[pixel-1])
		if showVal:
			if txt != '':
				txt += '\n'
			if rounding is not None:
				if rounding == 'int':
					txt += str(int(values[pixel-1]))
				else:
					txt += str(round(values[pixel-1], rounding))
		if txt!='':
			ax.text(horPosition-size/2, verPosition, txt, ma='center', va='center')
	#axColor = plt.axes([size*6, size*-6, size, size*])
	#plt.colorbar(scalarMap, cax = axColor, orientation="vertical")
	fig.colorbar(scalarMap, ax=ax)
	plt.axis('off')
	if title is not None:
		plt.title(title)
	if filename is not None:
		plt.savefig(filename)
		if saveDontShow == True:
			plt.clf()
			return
	else:
		if saveDontShow == True:
			print('need to pass a name if you want to save the file')
			plt.clf()
			return
		plt.show()
		return
	return

def makeHitsOverTimeGif(timestamps, hitlocations, outputname, batchsize = None, units = 'daq', cumulative = False, cmap = 'cividis', size = 3, showNum = True, showVal = True, alpha=1, rounding = None, norm = None, forceMin = None, labels=None, duration = 300):
	#this function basically does the same thing as plot one detector but it goes through and does it by saving the output of that function to .pngs and combining them into a .gif
	#it expects the hit locations to be passed as the actual pixel hit values not the board and channel
	startTime = np.min(timestamps)
	steps = None
	numSteps = None
	if batchsize is None:
		#default value of 10 batches basically
		numSteps = 10
		stopTime = np.max(timestamps)
		stepSize = (stopTime - startTime)/10.0
		steps = []
		for i in range(numSteps):
			steps.append(startTime + i * stepSize)
	else:
		if isinstance(batchsize, str): #parse the batches this way
			#if this is the case, then the last value is supposed to be the indicator of the unit
			unit = batchsize[-1]
			number = int(batchsize[:-1])
			#with this information, convert things into seconds
			if unit == 's':
				stepSize = number
			elif unit == 'm':
				stepSize = 60.0*number
			elif unit == 'h':
				stepSize = 3600.0*number
			else:
				print('unrecognized unit of time')
				print('expects s, m, or h')
				return None
			#check the units on the timestamps
			if units == 'daq':
				stepSize = stepSize / (4.0E-9)
			elif units == 's':
				stepSize = stepSize
			numSteps = int((np.max(timestamps)-startTime)/stepSize)
			steps = []
			for i in range(numSteps):
				steps.append(startTime + i * stepSize)
		elif isinstance(batchsize, int): #want this many batches
			stopTime = np.max(timestamps)
			stepSize = (stopTime - startTime)/batchsize
			steps = []
			for i in range(batchsize):
				steps.append(startTime + i * stepSize)
		elif isinstance(batchsize, (list, np.ndarray)): #provided the regions
			steps = list(batchsize)
		else:
			print('unsupported batchsize argument')
			print('expects intu (u is unit of s, m, or h), integer, list, or 1d np.ndarray')
			return None
	numSteps = len(steps)
	batches = []
	if numSteps - 1 == 0:
		batches.append(np.histogram(hitvals, np.arange(1, 128))[0])
	else:
		for i in range(numSteps-1):
			hitvals = None
			if cumulative:
				hitvals = hitlocations[np.less(timestamps, steps[i+1])]
			else:
				hitvals = hitlocations[np.logical_and(np.greater_equal(timestamps, steps[i]), np.less(timestamps, steps[i+1]))]
			batch = np.histogram(hitvals, np.arange(1, 129))[0]
			batches.append(batch)
	#now that the histograms are prepared, need to do the actual plotting
	batches = np.array(batches)
	minVal = np.min(batches)
	maxVal = np.max(batches)
	if forceMin is not None:
		minVal = forceMin
	#make a temporary directory for the animation
	tempdir = tempfile.TemporaryDirectory()
	frames = []
	for i in tqdm(range(len(batches))):
		title = 'Frame '+str(i)+' out of '+str(len(steps))
		filename = os.path.join(tempdir.name, str(i)+'.png')
		plotOneDetector(batches[i], numDet = 1, cmap = cmap, size = size, showNum = showNum, showVal = showVal, alpha=alpha, rounding = rounding, title = title, norm = norm, forceMin = minVal, forceMax = maxVal, labels=labels, filename = filename, saveDontShow = True)
		plt.close()
		frames.append(Image.open(filename))
	frames[0].save(outputname, format='GIF', append_images=frames[1:], save_all=True, duration=duration, loop=0)
	tempdir.cleanup()
