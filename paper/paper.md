---
title: 'nabPy : An Analysis Library for the Nab Experiment' 
tags:
  - h5py
  - HDF5
  - signal processing
  - dask
  - CUDA
authors:
  - name: D. G. Mathews 
    orcid: 0000-0002-4897-4379
    affiliation: "1, 2" # (Multiple affiliations must be quoted)
    corresponding: true
affiliations:
 - name: Oak Ridge National Laboratory
   index: 1
 - name: University of Kentucky
   index: 

date: 26 March 2024
bibliography: paper.bib

# Optional fields if submitting to a AAS journal too, see this blog post:
# https://blog.joss.theoj.org/2018/12/a-new-collaboration-with-aas-publishing
aas-doi: 10.3847/xxxxx <- update this with the DOI from AAS once you know it.
aas-journal: Astrophysical Journal <- The name of the AAS journal.
---

[^1]: Notice: This manuscript has been authored by UT-Battelle, LLC, under contract DE-AC05-00OR22725 with the US Department of Energy (DOE). The US government retains and the publisher, by accepting the article for publication, acknowledges that the US government retains a nonexclusive, paid-up, irrevocable, worldwide license to publish or reproduce the pub-lished form of this manuscript, or allow others to do so, for US government purposes. DOE will provide public access to these results of federally sponsored research in accordance with the DOE Public Access Plan (http://energy.gov/downloads/doe-public-access-plan).

# Summary

nabPy is an analysis library developed by and for the Nab collaboration [@nab]. It started as a simple file reading system intended to help collaborators prase the output from the data acquisition system [@david_2022], and has evolved over time into a robust signal processing focusedd library. It presently supports a variety of signal processing tasks, from standard cuts on data, to energy and timing extraction with FIR filers such as the trapezoidal and cusp filters. Additionally, many analysis tasks are enabled on both CPU and GPU through the Dask [@dask] and Cupy [@cupy] libraries. The library is based around a series of classes backed by Dask and Pandas [@pandas], each intended for parsing and analyzing a different type of data collected by the Nab experiment.

# Statement of Need

The Nab experiment is expected to record several petabytes of experimental data, mostly comprised of 1-dimensional detector signals. The analysis of this dataset will require routines that are both high-thoughput, in order to handle the raw amount of data being produced, and highly adjustable, to adapt for any changes in data structure and signal features over the course of the experiment. As such, a Python-based analysis suite was developed that is backed by high-performance libraries such as Numpy, Pandas, Dask, and Cupy to support the analysis needs of the experiment. While this library is specifically tailored to process Nab datafiles the analysis functions, such as the signal processing routines, are written externally to the Nab-specific classes to allow for generalization to other datasets.

# Parsing of Data Files

nabPy is based around a series of classes, each of which handles a specific type of data. For example, the waveformFile class is responsible for handling the data saved in the waveforms. Initially, the data output from the Nab data acquisition system was output into separate binary files, each of which was parsed differently, hence the different classes. In the most recent forms of the DAQ, the data is output as individually HDF5 [@hdf5]formatted files and handled with the h5py library [@h5py] and each class is responsible for parsing particular sections of those files.

# Signal Processing Capabilities

For more details on the signal processing features of nabPy, see the official documentation here: [INSERT URL EVENTUALLY].